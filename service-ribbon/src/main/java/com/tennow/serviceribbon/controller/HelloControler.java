package com.tennow.serviceribbon.controller;


import com.tennow.serviceribbon.service.HelloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: Wang Huiqiang
 * @Date: 2019/6/22
 */
@RestController
public class HelloControler {

    @Autowired
    HelloService helloService;

    @GetMapping("/hi")
    public String hi(@RequestParam String name) {
        return helloService.hiService(name);
    }

    @GetMapping("/test")
    public String test() {
        return "This is test!";
    }
}
