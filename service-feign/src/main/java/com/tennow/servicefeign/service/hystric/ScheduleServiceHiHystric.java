package com.tennow.servicefeign.service.hystric;

import com.tennow.servicefeign.service.ScheduleServiceHi;
import org.springframework.stereotype.Component;

/**
 * @Author: Wang Huiqiang
 * @Date: 2019/6/22
 */
@Component
public class ScheduleServiceHiHystric implements ScheduleServiceHi {
    @Override
    public String sayHiFromClientOne(String name) {
        return "sorry " + name;
    }
}
